import numpy as np
import time
from datetime import datetime,date
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import time
import scipy.signal as signal
from datetime import datetime,date
import matplotlib.pyplot as plt
import pandas as pd

import seaborn as sns # visualization



df = pd.read_csv("rssi_comp.csv")


x_axis = df['x']
y_axis = df['y']
z_axis = df['z']
rssi = df['rssi']
timestamp = df['timestamp']

x_calib_mean = np.mean(x_axis[1:20])
x_calib = x_axis - x_calib_mean
x_calib = x_calib[:]
timestamp = timestamp[:]

y_calib_mean = np.mean(y_axis[1:20])
y_calib = y_axis - y_calib_mean
y_calib = y_calib[:]
timestamp = timestamp[:]

z_calib_mean = np.mean(y_axis[1:20])
z_calib = z_axis - z_calib_mean
z_calib = z_calib[:]
timestamp = timestamp[:]

dt = (timestamp[len(timestamp)-1] - timestamp[0]) / len(timestamp)

## Computing Velocity and Position along Y Axis:
y_vel = [0]
for i in range(len(y_calib)-1):
    y_vel.append(y_vel[-1] + dt * y_calib[i])

y = [0]

for i in range(len(y_vel)-1):
    y.append(y[-1] + dt * y_vel[i])
    
## Integrations along X axis
x_vel = [0]
for i in range(len(x_calib)-1):
    x_vel.append(x_vel[-1] + dt * x_calib[i])


x = [0]

for i in range(len(x_vel)-1):
    x.append(x[-1] + dt * x_vel[i])


z_vel = [0]
for i in range(len(z_calib)-1):
    z_vel.append(z_vel[-1] + dt * z_calib[i])

#plt.plot(z_vel)

z = [0]
for i in range(len(z_vel)-1):
    z.append(z[-1] + dt * z_vel[i])




accel_raw = np.linalg.norm(np.array([x_calib, y_calib, z_calib]), axis=0)
accel = signal.savgol_filter(accel_raw, window_length=11, polyorder=4)

min_threshold = 0.40  ## Change the threshold (if needed) based on the peak accelerometer values that you observe in your plot above

# Calculate the upper threshold for peak detection as the maximum value in the data
upper_threshold = np.max(accel)

# Define the range for peak detection
my_range = (min_threshold, upper_threshold)

# print("range of Accel. values  for peak detection",my_range)
## Visualize the detected peaks in the accelerometer readings based on the selected range


#Use this link to find the peaks: https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.find_peaks.html
peak_array, k = signal.find_peaks(accel,prominence=0.01,width=2)
left_bases = k['left_bases']
right_bases = k['right_bases']
# print(peak_array)# Enter function here
# plt.plot(peak_array, accel[peak_array], "x", color="r")
# plt.scatter(left_bases, accel[left_bases], color='orange')
# plt.scatter(right_bases, accel[right_bases], color='green')



# print(k)
# plt.ylabel("Acceleration in m/s^2")
# plt.xlabel("Time in s")
# plt.title("Acceleration vs time")
# plt.show()

# Peak array indices -> peak_array
# Accel values at high peaks --> accel[peak_array]


# Set the orientation/direction of motion (walking direction).
# walking_direction is an angle in degrees with global frame x-axis. It can be from 0 degrees to 360 degrees.
# for e.g. if walking direction is 90 degrees, user is walking in the positive y-axis direction.
# Assuming you are moving along the +X-axis with minor deviations/drifts in Y, we set the orientation to 5 (ideally it should be 0 but to take into account the drifts we keep 5)
# Additionally, we assume that the walking direction will be the same throught the trajectory that you capture in exercise 1.

# This will change for exercise 2
walking_dir = np.deg2rad(5) ## deg to radians


# To compute the step length, we estimate it to be propertional to the height of the user.

height=1.32 # in meters # Change the height of the user as needed
step_length= 0.415 * height # in meters

# Convert walking direction into a 2D unit vector representing motion in X, Y axis:
angle = np.array([np.cos(walking_dir), -np.sin(walking_dir)])




x_angle = np.array(x)[peak_array]
y_angle = np.array(y)[peak_array]
z_angle = np.array(z)[peak_array]
## Start position of the user i.e. (0,0)
cur_position = np.array([0.0, 0.0], dtype=float)
t = []
for i in range(len(peak_array)):
    t.append(cur_position)
    angle =np.array([np.cos(z_angle[i]), np.sin(z_angle[i])])

    cur_position = cur_position + step_length * angle

t = np.array(t)
# Trajectory positions are stored in t, plot it for the first checkpoint
x2 = [i[0] for i in t]
y2 = [i[1] for i in t]


fig, ax = plt.subplots(3, 1)
ax[0].plot(timestamp,x, label="X positions", c="red")
ax[0].plot(timestamp,y, label="Y positions", c="green")
ax[0].plot(timestamp, z, label="Z positions", c ="blue")
ax[0].legend(loc="upper left")
ax[0].set_xlabel("Timestamp (seconds)")
ax[0].set_ylabel("Positions (m)")

ax[1].plot(accel)
ax[1].plot(peak_array, accel[peak_array], "x", color="r")
ax[1].scatter(left_bases, accel[left_bases], color='orange')
ax[1].scatter(right_bases, accel[right_bases], color='green')
ax[1].set_ylabel("Acceleration in m/s^2")
ax[1].set_xlabel("Time in s")
ax[1].set_title("Acceleration vs time")
for i in range(len(peak_array)):
    ax[1].text(peak_array[i], accel[peak_array][i], str(i + 1))



ax[2].plot(x2, y2)
ax[2].scatter(x2, y2, c=rssi[peak_array])
ax[2].set_xlabel("X")
ax[2].set_ylabel("Y")
ax[2].set_title("Trajectory")
plt.show()



new_array = np.array(rssi[peak_array])
possible_x = x2
possible_y = y2
max_indices = np.argsort(new_array)[::-1][:5]

max_xs = np.array(possible_x)[max_indices]
max_ys = np.array(possible_y)[max_indices]
tmp_rssi = [100 + x for x in new_array]
tmp_sum = np.sum(tmp_rssi)


#weights = [ (i / tmp_sum) * 5 for i in tmp_rssi]
#print(weights)
weights = [2, 2, 0.5, 0.25, 0.25] 
centroid_x = np.mean([weights[i] * max_xs[i] for i in range(5)])
centroid_y = np.mean([weights[i] * max_ys[i] for i in range(5)])
print(centroid_y, centroid_x)
plt.scatter(max_xs, max_ys, c=new_array[max_indices])
plt.scatter(centroid_x, centroid_y, c ='red')
plt.show()


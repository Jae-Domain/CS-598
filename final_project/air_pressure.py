from sense_hat import SenseHat
from time import sleep, time
from datetime import datetime
sense = SenseHat()
sense.clear()


# This Pressure is in milibars.
# When we get the sesnor, units will change

path = "co2_readings.csv"
interval = 3

with open(path, 'w') as fp:
    fp.write("Timestamp, Reading\n")
    fp.close()

while True:
    pressure = sense.get_pressure()
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    print(current_time, pressure)
    
    with open(path, 'a') as fp:
        fp.write(str(current_time) + ", " + str(pressure) + "\n")
        fp.close()
    
    sense.clear()
    sleep(interval)
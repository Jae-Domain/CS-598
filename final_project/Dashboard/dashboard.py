import dash
from dash import html, dcc, Input, Output, dash_table
import plotly.express as px
import pandas as pd
import random
from utils import get_CO2_Readings

# Initialize the DataFrame with columns
data_columns = ['Time', 'CO2', 'Temperature', 'Humidity', 'Status']
data_df = pd.DataFrame(columns=data_columns)

app = dash.Dash(__name__)

app.layout = html.Div([
    dcc.Interval(id='interval-component', interval=600000, n_intervals=0),
    html.H1("Vehicle Interior Dashboard"),
    dcc.Tabs(id="tabs", value='tab-co2', children=[
        dcc.Tab(label='CO2 Levels', value='tab-co2'),
        dcc.Tab(label='Temperature', value='tab-temp'),
        dcc.Tab(label='Humidity', value='tab-humidity'),
        dcc.Tab(label='Status', value='tab-status'),
        dcc.Tab(label='All Data', value='tab-data')
    ]),
    html.Div(id='tabs-content')
])

@app.callback(
    Output('tabs-content', 'children'),
    Input('tabs', 'value'),
    Input('interval-component', 'n_intervals')
)
def render_content(tab, n):
    global data_df
    new_data = get_CO2_Readings()


    # Append new data to DataFrame
    
    if new_data != None:
        data_df = pd.concat([data_df, pd.DataFrame([new_data])], ignore_index=True)
        
        if tab == 'tab-co2':
            co2_fig = px.line(data_df, x='Time', y='CO2', title='CO2 Levels Over Time')
            return dcc.Graph(figure=co2_fig)
        elif tab == 'tab-temp':
            temp_fig = px.line(data_df, x='Time', y='Temperature', title='Temperature Over Time')
            return dcc.Graph(figure=temp_fig)
        elif tab == 'tab-humidity':
            humidity_fig = px.line(data_df, x='Time', y='Humidity', title='Humidity Over Time')
            return dcc.Graph(figure=humidity_fig)
        elif tab == 'tab-status':
            return html.Div([
                html.H3('Current Status'),
                html.Div(f"Status: {new_data['Status']}", style={'fontSize': 24})
            ])
        elif tab == 'tab-data':
            return dash_table.DataTable(
                data=data_df.to_dict('records'),  # Display the last 10 records
                columns=[{'name': i, 'id': i} for i in data_columns],
                page_size=10  # Enable pagination
            )   

if __name__ == '__main__':
    app.run_server(debug=True)

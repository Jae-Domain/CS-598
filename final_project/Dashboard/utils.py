import pandas as pd
import random 

def get_CO2_Readings():
    return {
        'Time': pd.Timestamp.now(),
        'CO2': random.randint(400, 1000),
        'Temperature': random.uniform(20, 30),
        'Humidity': random.uniform(30, 60),
        'Status': random.choice(['Dog', 'Human', 'Empty'])
    }
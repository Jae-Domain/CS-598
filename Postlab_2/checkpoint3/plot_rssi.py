import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

df = pd.read_csv('output.csv')
df2 = pd.read_csv('joystick.csv')

directions = df2.iloc[:, 1]

#rssi_values = [df.iloc[:, -1][t] for t in df2.iloc[:, 0]]
#print(rssi_values)

joystick_timestamps = np.array([np.longdouble(t) for t in df2.iloc[:, 0]])
rssi_timestamps = np.array([np.longdouble(t) for t in df.iloc[:, 0]])
rssi_vals = [df.iloc[0, -1]]

for t in joystick_timestamps:
    reduced = np.argmin(abs(rssi_timestamps - t))
    rssi_vals.append(df.iloc[reduced, -1])

start = (0, 0)
coordinates = [start]

for dir in directions:
    if dir == "up":
        coordinates.append((start[0], start[1] + 1))
        start = (start[0], start[1] + 1)
    elif dir == "down":
        coordinates.append((start[0], start[1] - 1))
        start = (start[0], start[1] - 1)
    elif dir == "left":
        coordinates.append((start[0] - 1, start[1]))
        start = (start[0] - 1, start[1])
    elif dir == "right":
        coordinates.append((start[0] + 1, start[1]))
        start = (start[0] + 1, start[1])

x = np.array([i[0] for i in coordinates])
y = np.array([i[1] for i in coordinates])


        
        
plt.scatter(x, y, c=rssi_vals)
plt.savefig('walkingpath.png')
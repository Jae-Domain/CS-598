import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

df = pd.read_csv('output.csv')
df2 = pd.read_csv('joystick.csv')


plt.plot(((df.iloc[:, 0])), df.iloc[:, -1])
plt.scatter(df2.iloc[:, 0], [-40]* 3, color='red')
plt.savefig('rssi_plot_with_joystick_pressed.png')
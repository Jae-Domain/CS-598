from sense_hat import SenseHat
from time import sleep, time
import csv




def write_to_file(file_name, data):
    """Write data to a file"""
    with open(file_name, "a") as f:
        f.write(data)
        f.close()



def record_joystick():



    sense=SenseHat()


    while True:
        for event in sense.stick.get_events():
            
            if event.action =="pressed":
                timestamp = time()## check if the joystick was pressed
                
                data = str(timestamp) + ", " + str(event.direction) + "\n"
                write_to_file("joystick.csv", data)
                
                
                sleep(2) ## wait a while and then clear the screen
                sense.clear()
            
            
    
    return


record_joystick()
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from scipy.signal import chirp, find_peaks, peak_widths, savgol_filter, find_peaks_cwt
from scipy.fft import fft, fftfreq
df = pd.read_csv('../output.csv')


#max = df.iloc[:, -1].idxmax()
y = df.iloc[:, -1]
x = df.iloc[:, 0]
#ma = savgol_filter(x, 50, 4)
ma = savgol_filter(y, 50, 4)


peaks, _ = find_peaks(y, height=-50)
print(peaks)



#identify peak
peak_x = 5
peak_y = -10
length = 5
width = 5
plt.plot(np.arange(len(df.iloc[:, 0])), df.iloc[:, -1])
plt.axvspan(peaks[0], peaks[-1], color='red', alpha=0.5)
plt.ylabel('RSSI')
plt.xlabel('Time')
plt.savefig('rssi_plot_with_peak.png')



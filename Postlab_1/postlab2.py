from sense_hat import SenseHat
from time import sleep
import time
import datetime

import numpy as np
import time
from picamera2 import Picamera2
from picamera2.encoders import H264Encoder
from picamera2.outputs import CircularOutput
from libcamera import controls


import cv2

def facial_recognition():
    picam2=Picamera2()  ## Create a camera object
    sense=SenseHat()
    dispW=1280
    dispH=720
    ## Next, we configure the preview window size that determines how big should the image be from the camera, the bigger the image the more the details you capture but the slower it runs
    ## the smaller the size, the faster it can run and get more frames per second but the resolution will be lower. We keep 
    picam2.preview_configuration.main.size= (dispW,dispH)  ## 1280 cols, 720 rows. Can also try smaller size of frame as (640,360) and the largest (1920,1080)
    ## with size (1280,720) you can get 30 frames per second

    ## since OpenCV requires RGB configuration we set the same format for picam2. The 888 implies # of bits on Red, Green and Blue
    picam2.preview_configuration.main.format= "RGB888"
    picam2.preview_configuration.align() ## aligns the size to the closest standard format
    picam2.preview_configuration.controls.FrameRate=30 ## set the number of frames per second, this is set as a request, the actual time it takes for processing each frame and rendering a frame can be different

    picam2.configure("preview")
    faceCascade=cv2.CascadeClassifier("/home/pi/Desktop/Lab1/Part 2/haarcascade_frontalface_default.xml")

    sense.clear() ## to clear the LED matrix


    init_temperature=sense.get_temperature()
    init_temperature=round(init_temperature,1)  ## round temperature to 1 decimal place

    init_humidity=round(sense.get_humidity(), 1)

    while True:
        sense.clear()
        temp = round(sense.get_temperature(), 1)
        hum = round(sense.get_humidity(), 1)
        print("temperature: ", temp, "humidity:", hum)
        
        if abs(temp - init_temperature) > 1 or abs(hum - init_humidity) > 1:
            picam2.start()
            while True:
                frame=picam2.capture_array() ## frame is a large 2D array of rows and cols and at intersection of each point there is an array of three numbers for RGB i.e. [R,G,B] where RGB value ranges from 0 to 255
                frameGray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            
                faces=faceCascade.detectMultiScale(frameGray, 1.3, 5)
                for face in faces:
                    x,y,w,h=face
                    cv2.rectangle(frame, (x,y), (x+w, y+h),(255,0,0),3)
                    time.sleep(0.5)
                cv2.imshow("piCamera2", frame) ## show the frame
                key=cv2.waitKey(5) & 0xFF
            
                if key == ord("q"): ## stops for 1 ms to check if key Q is pressed
                    cv2.destroyAllWindows()
                    return
                
        sleep(2)
            
    return

facial_recognition()
            